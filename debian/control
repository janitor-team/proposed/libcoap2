Source: libcoap2
Section: libs
Priority: optional
Maintainer: Debian IoT Maintainers <debian-iot-maintainers@lists.alioth.debian.org>
Uploaders:
 Carsten Schoenert <c.schoenert@t-online.de>,
Build-Depends:
 asciidoc,
 debhelper-compat (= 12),
 doxygen,
 exuberant-ctags,
 graphviz,
 libgnutls28-dev,
 libssl-dev,
 pkg-config,
 xmlto,
Standards-Version: 4.4.1
Homepage: https://libcoap.net/
Vcs-Browser: https://salsa.debian.org/debian-iot-team/libcoap2
Vcs-Git: https://salsa.debian.org/debian-iot-team/libcoap2.git

Package: libcoap2
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: C-Implementation of CoAP - libraries API version 2
 Lightweight application-protocol for devices that are constrained their
 resources such as computing power, RF range, memory, bandwidth, or network
 packet sizes. This protocol, CoAP, is developed in the IETF working group
 "CoRE", <http://6lowapp.net> and was standardized in RFC 7252.
 .
 The libcoap library v2 has DTLS functionality included based on TLS
 functions provided by OpenSSL in addition to the previous API version.
 .
 This package contains the various libcoap libraries based on API v2 with
 and without DTLS functionality.

Package: libcoap2-bin
Architecture: any
Depends:
 libcoap2 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts: libcoap-1-0-bin,
Description: C-Implementation of CoAP - example binaries API version 2
 The libcoap provides some example binary files to show the usage of a CoAP
 Server, CoAP Client and a Resource Directory implementation. These examples
 heavily rely on the libcoap as the core functionality is based there.
 .
 This package provides the following example binaries without DTLS functions:
  * coap-client
    A command-line client that allows you to interact with CoAP reasources.
 .
  * coap-server
    Simple server that can provide CoAP resources for simple testing cases.
 .
  * coap-rd
    This binary let you simulate various CoAP resources mostly for testing
    purpose.
 .
 This package providesthe following example binaries with DTLS functions:
  * coap-client-openssl
    A command-line client that allows you to interact with CoAP reasources
    based on TLS functions by OpenSSL.
 .
  * coap-server-openssl
    Simple server that can provide CoAP resources for simple testing cases
    based on TLS functions by OpenSSL.
 .
  * coap-rd-openssl
    This binary let you simulate various CoAP resources mostly for testing
    purpose based on TLS functions by OpenSSL.
 .
 You can use these files without any exception no matter as they named
 examples. Please see the respectives man pages for usage instructions.

Package: libcoap2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libc6-dev,
 libcoap2 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: C-Implementation of CoAP - development files API version 2
 Lightweight application-protocol for devices that are constrained their
 resources such as computing power, RF range, memory, bandwidth, or network
 packet sizes. This protocol, CoAP, is developed in the IETF working group
 "CoRE", <http://6lowapp.net> and was standardized in RFC 7252.
 .
 This package contains the developer files like headers, the static library
 and the pkg-config file.

Package: libcoap2-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 libjs-jquery,
 ${misc:Depends},
Recommends:
 libcoap2-dev (= ${binary:Version}),
 www-browser,
Description: C-Implementation of CoAP - HTML based documentation files for API v2
 Lightweight application-protocol for devices that are constrained their
 resources such as computing power, RF range, memory, bandwidth, or network
 packet sizes. This protocol, CoAP, is developed in the IETF working group
 "CoRE", <http://6lowapp.net> and was standardized in RFC 7252.
 .
 This package contains the Doxygen generated library API HTML documentation
 for libcoap.
 They can be accessed by open /usr/share/doc/libcoap2-doc/html/index.html.
